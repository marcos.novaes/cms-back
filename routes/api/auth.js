const express = require('express');
const { body, validationResult } = require('express-validator');
const router = express.Router();
const bcrypt = require('bcryptjs');
const Usuario = require('../../models/user')
const jwt = require('jsonwebtoken')
const config = require('config')


router.post('/', [
    body('email').isEmail().withMessage('Email tem que ser num formato valido'),
    body('password').not().isEmpty().withMessage('Senha é necessaria')
], async (req, res, next) => {

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() })
    }
    const jwtSecret = process.env.jwtSecret || config.get('jwtSecret')
    const { email, password } = req.body
    try {
        let user = await Usuario.findOne({ email }).select('id password email name')
        if (!user) {
            return res.status(404).json({ errors: 'Usuario nao encontrado' })
        } else {
            const isMatch = await bcrypt.compare(password, user.password);
            if (!isMatch) {
                return res.status(400).json({ errors: 'Senha inválida' });
            } else {
                const payload = {
                    user: {
                        id: user.id,
                        name: user.name,
                        email: user.email
                    }
                }
                jwt.sign(payload, jwtSecret, { expiresIn: '5 days' },
                    (err, token) => {
                        if (err) throw err;
                        payload.token = token
                        return res.status(200).json(payload);
                    }
                );
            }
        }

    } catch (err) {
        console.error(err.message)
        res.status(500).send('Server error')
    }
})




module.exports = router;