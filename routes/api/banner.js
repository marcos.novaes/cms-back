const express = require('express');
const { body, validationResult } = require('express-validator');
const router = express.Router();
const Banner = require('../../models/banner')
const auth = require('../../middleware/auth')
const file = require('../../middleware/file')



router.post('/', auth, file, [
    body('title').not().isEmpty().withMessage('Nome tem que ser preenchido'),
    body('description').not().isEmpty().withMessage('Descricao tem que ser preenchida')
], async (req, res, next) => {
    try {
        let { title, photo, description } = req.body
        const erros = validationResult(req)
        if (!erros.isEmpty()) {
            return res.status(400).json({ erros: erros.array() })
        } else {
            const last_modified_by = req.user.id
            let banner = new Banner({ title, photo, description, last_modified_by })
            await banner.save()
            if (banner.id) {
                return res.status(200).json(banner)
            }
        }
    } catch (err) {
        console.error(err.message)
        res.status(500).send({ "error": 'Erro na realizaçao da requisiçao' })
    }
})

router.get('/:id', auth, async (req, res, next) => {
    try {
        let id = req.params.id
        const banner = await Banner.findById({ _id: id })
        if (banner) {
            return res.status(200).json(banner)
        } else {
            return res.status(404).json({ erros: 'banner nao encontrado' })
        }
    } catch (err) {
        console.error(err.message)
        res.status(500).send({ "error": 'Erro na realizaçao da requisiçao' })
    }
})

router.get('/', auth, async (req, res, next) => {
    try {
        let banners = await Banner.find()
        if (banners.length > 0) {
            return res.status(200).json(banners)
        } else {
            return res.status(404).json({ erros: "Nao tem banners" })
        }
    } catch (err) {
        console.error(err.message)
        res.status(500).send({ "error": 'Erro na realizaçao da requisiçao' })
    }
})

router.delete('/:id', auth, async (req, res, next) => {
    try {
        let id = req.params.id
        const banner = await Banner.findByIdAndDelete(id)
        if (banner) {
            return res.status(200).json(banner)
        } else {
            return res.status(404).json({ erros: 'banner nao encontrado' })
        }
    } catch (err) {
        console.error(err.message)
        res.status(500).send({ "error": 'Erro na realizaçao da requisiçao' })
    }
})

router.patch('/:id', auth, file, async (req, res, next) => {
    try {

        let bodyRequest = req.body
        bodyRequest.last_modified_by = req.user.id
        const id = req.params.id
        const update = { $set: bodyRequest }
        const banner = await Banner.findByIdAndUpdate(id, update, { new: true })
        if (banner) {
            res.send(banner)
        } else {
            res.status(404).send({ erros: 'banner nao encontrado' })
        }
    } catch (err) {
        console.error(err.message)
        res.status(500).send({ "error": 'Erro na realizaçao da requisiçao' })
    }
})



module.exports = router;