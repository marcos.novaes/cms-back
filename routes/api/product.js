const express = require('express');
const { body, validationResult } = require('express-validator');
const router = express.Router();
const Produto = require('../../models/product')
const auth = require('../../middleware/auth')
const file = require('../../middleware/file')



router.post('/', auth, file, [
    body('title').not().isEmpty().withMessage('Nome tem que ser preenchido'),
    body('description').not().isEmpty().withMessage('Descricao tem que ser preenchida'),
    body('complete_description').not().isEmpty().withMessage('Descricao completa tem que ser preenchida'),
    body('price').not().isEmpty().withMessage('preço tem que ser preenchido'),
    body('discount_price').not().isEmpty().withMessage('preço com desconto tem que ser preenchido'),
    body('discount_price_percent').not().isEmpty().withMessage('percentual de desconto tem que ser preenchido')
], async (req, res, next) => {
    try {
        let { title, photo, description, complete_description, price, discount_price, discount_price_percent, status } = req.body
        const erros = validationResult(req)
        if (!erros.isEmpty()) {
            return res.status(400).json({ erros: erros.array() })
        } else {
            const last_modified_by = req.user.id
            let product = new Produto({ title, photo, description, complete_description, price, discount_price, discount_price_percent, status, last_modified_by })
            await product.save()
            if (product.id) {
                return res.status(200).json(product)
            }
        }
    } catch (err) {
        console.error(err.message)
        res.status(500).send({ "error": 'Erro na realizaçao da requisiçao' })
    }
})

router.get('/:id', auth, async (req, res, next) => {
    try {
        let id = req.params.id
        const produto = await Produto.findById({ _id: id })
        if (produto) {
            return res.status(200).json(produto)
        } else {
            return res.status(404).json({ erros: 'produto nao encontrado' })
        }
    } catch (err) {
        console.error(err.message)
        res.status(500).send({ "error": 'Erro na realizaçao da requisiçao' })
    }
})

router.get('/', auth, async (req, res, next) => {
    try {
        let products = await Produto.find()
        if (products.length > 0) {
            return res.status(200).json(products)
        } else {
            return res.status(404).json({ erros: "Nao tem produtos" })
        }
    } catch (err) {
        console.error(err.message)
        res.status(500).send({ "error": 'Erro na realizaçao da requisiçao' })
    }
})

router.delete('/:id', auth, async (req, res, next) => {
    try {
        let id = req.params.id
        const produto = await Produto.findByIdAndDelete(id)
        if (produto) {
            return res.status(200).json(produto)
        } else {
            return res.status(404).json({ erros: 'Produto nao encontrado' })
        }
    } catch (err) {
        console.error(err.message)
        res.status(500).send({ "error": 'Erro na realizaçao da requisiçao' })
    }
})

router.patch('/:id', auth, file, async (req, res, next) => {
    try {

        let bodyRequest = req.body
        bodyRequest.last_modified_by = req.user.id
        const id = req.params.id
        const update = { $set: bodyRequest }
        const produto = await Produto.findByIdAndUpdate(id, update, { new: true })
        if (produto) {
            res.send(produto)
        } else {
            res.status(404).send({ erros: 'Produto nao encontrado' })
        }
    } catch (err) {
        console.error(err.message)
        res.status(500).send({ "error": 'Erro na realizaçao da requisiçao' })
    }
})



module.exports = router;