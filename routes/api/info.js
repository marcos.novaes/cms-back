const express = require('express');
const { body, validationResult } = require('express-validator');
const router = express.Router();
const Info = require('../../models/info')
const auth = require('../../middleware/auth')




router.post('/', auth, [
    body('name').not().isEmpty().withMessage('Nome tem que ser preenchido'),
    body('description').not().isEmpty().withMessage('Descricao tem que ser preenchida'),
    body('icon').not().isEmpty().withMessage('Icon  tem que ser preenchido')
], async (req, res, next) => {
    try {
        let { name, description, icon } = req.body
        const erros = validationResult(req)
        if (!erros.isEmpty()) {
            return res.status(400).json({ erros: erros.array() })
        } else {
            const last_modified_by = req.user.id
            let info = new Info({ name, description, icon, last_modified_by })
            await info.save()
            if (info.id) {
                return res.status(200).json(info)
            }
        }
    } catch (err) {
        console.error(err.message)
        res.status(500).send({ "error": 'Erro na realizaçao da requisiçao' })
    }
})

router.get('/:id', auth, async (req, res, next) => {
    try {
        let id = req.params.id
        const info = await Info.findById({ _id: id })
        if (info) {
            return res.status(200).json(info)
        } else {
            return res.status(404).json({ erros: 'info nao encontrado' })
        }
    } catch (err) {
        console.error(err.message)
        res.status(500).send({ "error": 'Erro na realizaçao da requisiçao' })
    }
})

router.get('/', auth, async (req, res, next) => {
    try {
        let infos = await Info.find()
        if (infos.length > 0) {
            return res.status(200).json(infos)
        } else {
            return res.status(404).json({ erros: "Nao tem infos" })
        }
    } catch (err) {
        console.error(err.message)
        res.status(500).send({ "error": 'Erro na realizaçao da requisiçao' })
    }
})

router.delete('/:id', auth, async (req, res, next) => {
    try {
        let id = req.params.id
        const info = await Info.findByIdAndDelete(id)
        if (info) {
            return res.status(200).json(info)
        } else {
            return res.status(404).json({ erros: 'info nao encontrado' })
        }
    } catch (err) {
        console.error(err.message)
        res.status(500).send({ "error": 'Erro na realizaçao da requisiçao' })
    }
})

router.patch('/:id', auth, async (req, res, next) => {
    try {

        let bodyRequest = req.body
        bodyRequest.last_modified_by = req.user.id
        const id = req.params.id
        const update = { $set: bodyRequest }
        const info = await Info.findByIdAndUpdate(id, update, { new: true })
        if (info) {
            res.send(info)
        } else {
            res.status(404).send({ erros: 'info nao encontrado' })
        }
    } catch (err) {
        console.error(err.message)
        res.status(500).send({ "error": 'Erro na realizaçao da requisiçao' })
    }
})



module.exports = router;