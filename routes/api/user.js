const express = require('express');
const { body, validationResult } = require('express-validator');
const router = express.Router();
const bcrypt = require('bcryptjs');
const Usuario = require('../../models/user')
const auth = require('../../middleware/auth')


router.post('/', auth, [
    body('name').not().isEmpty().withMessage('Nome tem que ser preenchido'),
    body('email').isEmail().withMessage('Email tem que ser num formato valido'),
    body('password').isLength({ min: 6 }).withMessage('Senha tem que ter 6 digitos')
], async (req, res, next) => {
    try {
        let { name, email, password } = req.body
        const erros = validationResult(req)
        if (!erros.isEmpty()) {
            return res.status(400).json({ erros: erros.array() })
        } else {
            const salt = await bcrypt.genSalt(10)
            let user = new Usuario({ name, email, password })
            user.password = await bcrypt.hash(password, salt)
            await user.save()
            if (user.id) {
                return res.status(200).json(user)
            }
        }
    } catch (err) {
        console.error(err.message)
        res.status(500).send({ "error": 'Erro na realizaçao da requisiçao' })
    }
})

router.get('/:id', auth, async (req, res, next) => {
    try {
        let id = req.params.id
        const user = await Usuario.findById({ _id: id })
        if (user) {
            return res.status(200).json(user)
        } else {
            return res.status(404).json({ erros: 'Usuario nao encontrado' })
        }
    } catch (err) {
        console.error(err.message)
        res.status(500).send({ "error": 'Erro na realizaçao da requisiçao' })
    }
})

router.get('/', auth, async (req, res, next) => {
    try {
        let users = await Usuario.find()
        if (users.length > 0) {
            return res.status(200).json(users)
        } else {
            return res.status(404).json({ erros: "Nao tem usuarios" })
        }
    } catch (err) {
        console.error(err.message)
        res.status(500).send({ "error": 'Erro na realizaçao da requisiçao' })
    }
})

router.delete('/:id', auth, async (req, res, next) => {
    try {
        let id = req.params.id
        const user = await Usuario.findByIdAndDelete(id)
        if (user) {
            return res.status(200).json(user)
        } else {
            return res.status(404).json({ erros: 'Usuario nao encontrado' })
        }
    } catch (err) {
        console.error(err.message)
        res.status(500).send({ "error": 'Erro na realizaçao da requisiçao' })
    }
})

router.patch('/:id', auth, async (req, res, next) => {
    try {

        let bodyRequest = req.body
        const id = req.params.id
        const salt = await bcrypt.genSalt(10)
        if (bodyRequest.password) {
            bodyRequest.password = await bcrypt.hash(bodyRequest.password, salt)
        }
        const update = { $set: bodyRequest }
        const user = await Usuario.findByIdAndUpdate(id, update, { new: true })
        if (user) {
            res.send(user)
        } else {
            res.status(404).send({ erros: 'Usuario nao encontrado' })
        }
    } catch (err) {
        console.error(err.message)
        res.status(500).send({ "error": 'Erro na realizaçao da requisiçao' })
    }
})



module.exports = router;