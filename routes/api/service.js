const express = require('express');
const { body, validationResult } = require('express-validator');
const router = express.Router();
const Servico = require('../../models/service')
const auth = require('../../middleware/auth')




router.post('/', auth, [
    body('name').not().isEmpty().withMessage('Nome tem que ser preenchido'),
    body('description').not().isEmpty().withMessage('Descricao tem que ser preenchida'),
    body('icon').not().isEmpty().withMessage('Icon  tem que ser preenchido')
], async (req, res, next) => {
    try {
        let { name, description, icon } = req.body
        const erros = validationResult(req)
        if (!erros.isEmpty()) {
            return res.status(400).json({ erros: erros.array() })
        } else {
            const last_modified_by = req.user.id
            let service = new Servico({ name, description, icon, last_modified_by })
            await service.save()
            if (service.id) {
                return res.status(200).json(service)
            }
        }
    } catch (err) {
        console.error(err.message)
        res.status(500).send({ "error": 'Erro na realizaçao da requisiçao' })
    }
})

router.get('/:id', auth, async (req, res, next) => {
    try {
        let id = req.params.id
        const service = await Servico.findById({ _id: id })
        if (service) {
            return res.status(200).json(service)
        } else {
            return res.status(404).json({ erros: 'serviço nao encontrado' })
        }
    } catch (err) {
        console.error(err.message)
        res.status(500).send({ "error": 'Erro na realizaçao da requisiçao' })
    }
})

router.get('/', auth, async (req, res, next) => {
    try {
        let services = await Servico.find()
        if (services.length > 0) {
            return res.status(200).json(services)
        } else {
            return res.status(404).json({ erros: "Nao tem serviços" })
        }
    } catch (err) {
        console.error(err.message)
        res.status(500).send({ "error": 'Erro na realizaçao da requisiçao' })
    }
})

router.delete('/:id', auth, async (req, res, next) => {
    try {
        let id = req.params.id
        const servico = await Servico.findByIdAndDelete(id)
        if (servico) {
            return res.status(200).json(servico)
        } else {
            return res.status(404).json({ erros: 'Servico nao encontrado' })
        }
    } catch (err) {
        console.error(err.message)
        res.status(500).send({ "error": 'Erro na realizaçao da requisiçao' })
    }
})

router.patch('/:id', auth, async (req, res, next) => {
    try {

        let bodyRequest = req.body
        bodyRequest.last_modified_by = req.user.id
        const id = req.params.id
        const update = { $set: bodyRequest }
        const servico = await Servico.findByIdAndUpdate(id, update, { new: true })
        if (servico) {
            res.send(servico)
        } else {
            res.status(404).send({ erros: 'Servico nao encontrado' })
        }
    } catch (err) {
        console.error(err.message)
        res.status(500).send({ "error": 'Erro na realizaçao da requisiçao' })
    }
})



module.exports = router;