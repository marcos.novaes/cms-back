const jwt = require('jsonwebtoken');
const config = require('config');

module.exports = function (req, res, next) {
    // Get token from header
    const token = req.header('x-auth-token');

    if (!token) {
        return res.status(401).json({ erros: 'Nao tem token' });
    }

    try {
        jwt.verify(token, config.get('jwtSecret'), (error, decoded) => {
            if (error) {
                return res.status(401).json({ erros: 'Token invalido' });
            }
            req.user = decoded.user;
            next();
        })

    } catch (err) {
        console.error(err);
        res.status(500).json({ erros: 'Ocorreu um erro no login' });
    }
}
