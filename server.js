const PORT = process.env.PORT || 3001
//dependencias
const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const connectDB = require('./config/db')
const fileUpload = require('express-fileupload')
const app = express()

//conecta no banco
connectDB()

//init middleware
app.use(cors())
app.use(express.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json())
app.use('/uploads', express.static('uploads'));
app.use(fileUpload({ createParentPath: true }));

//rotas
app.get('/', (req, res) => {
    res.send('pagina principal')
})

app.use('/user', require('./routes/api/user'))
app.use('/login', require('./routes/api/auth'))
app.use('/product', require('./routes/api/product'))
app.use('/service', require('./routes/api/service'))
app.use('/info', require('./routes/api/info'))
app.use('/banner', require('./routes/api/banner'))


app.listen(PORT, () => console.log(`server rodando em http://localhost:${PORT}`))