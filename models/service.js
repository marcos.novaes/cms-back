const mongoose = require('mongoose');

const ServiceSchema = new mongoose.Schema({
    icon: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    last_modified_by: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'user'
    },
    last_modification_date: {
        type: Date,
        default: Date.now
    }
}, { autoCreate: true })

module.exports = mongoose.model('service', ServiceSchema);
